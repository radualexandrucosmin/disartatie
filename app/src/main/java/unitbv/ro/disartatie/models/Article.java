package unitbv.ro.disartatie.models;

public class Article {

    String content;
    String articleWriter;
    String picture;
    Long time;

    public Article(String content, String articleWriter, String picture, Long time) {
        this.content = content;
        this.articleWriter = articleWriter;
        this.picture = picture;
        this.time = time;
    }

    public Article() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getArticleWriter() {
        return articleWriter;
    }

    public void setArticleWriter(String articleWriter) {
        this.articleWriter = articleWriter;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
