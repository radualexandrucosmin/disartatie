package unitbv.ro.disartatie.models;

/**
 * Created by radua on 4/12/2018.
 */

public class Den extends Marker{

    String name;
    String description;
    String id;


    public Den() {
        super();
    }

    public Den(String name, String description, double latitude, double longitude) {
        super(latitude, longitude);
        this.name = name;
        this.description = description;
        this.id = id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
