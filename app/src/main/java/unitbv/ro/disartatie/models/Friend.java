package unitbv.ro.disartatie.models;

/**
 * Created by radua on 4/2/2018.
 */

public class Friend {

    String friends_since;

    public Friend() {

    }

    public Friend(String date) {
        this.friends_since = date;
    }

    public String getFriends_since() {
        return friends_since;
    }

    public void setFriends_since(String friends_since) {
        this.friends_since = friends_since;
    }
}
