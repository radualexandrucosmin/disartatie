package unitbv.ro.disartatie.enums;

/**
 * Created by radua on 3/29/2018.
 */

public enum UserRelationship {
    REQUEST_SENT("REQUEST_SENT"),
    REQUEST_RECEIVED("REQUEST_RECEIVED"),
    NOT_FRIENDS("NOT_FRIENDS"),
    FRIENDS("FRIENDS"),
    OWN_PROFILE("OWN_PROFILE");

    UserRelationship(String friends) {
    }
}
