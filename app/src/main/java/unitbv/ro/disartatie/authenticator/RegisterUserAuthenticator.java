package unitbv.ro.disartatie.authenticator;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

import unitbv.ro.disartatie.interfaces.OperationResultInterface;

public final class RegisterUserAuthenticator {

    private OperationResultInterface operationResultInterface;

    public void setOperationResultInterface(OperationResultInterface operationResultInterface) {
        this.operationResultInterface = operationResultInterface;
    }


    public void registerUser(final String displayName, String email, String password, final Context context) {

        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()) {

                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                    String uid = currentUser.getUid();

                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                    String device_token = FirebaseInstanceId.getInstance().getToken();

                    HashMap<String, String> userMap = createUserDatabaseHashMap(device_token, displayName);

                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if(task.isSuccessful()){
                                operationResultInterface.onSuccess();


                            } else {
                                Log.e(RegisterUserAuthenticator.class.getName(), task.getResult().toString());
                                operationResultInterface.onFailure();
                            }

                        }
                    });


                } else {
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_LONG).show();

                }
            }
        }) ;
    }

    @NonNull
    private HashMap<String, String> createUserDatabaseHashMap(String device_token, String displayName) {
        HashMap<String, String> userMap = new HashMap<>();
        userMap.put("name", displayName);
        userMap.put("status", "Hi there I'm using Disartatie App.");
        userMap.put("image", "default");
        userMap.put("thumb_image", "default");
        userMap.put("device_token", device_token);
        return userMap;
    }

}
