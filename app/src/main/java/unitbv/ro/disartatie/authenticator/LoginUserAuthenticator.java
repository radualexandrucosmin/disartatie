package unitbv.ro.disartatie.authenticator;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.abstracts.AbstractAppCompatActivity;
import unitbv.ro.disartatie.interfaces.OperationResultInterface;

import static android.content.ContentValues.TAG;

public final class LoginUserAuthenticator {

    private OperationResultInterface mOperationResultInterface ;
    private FirebaseAuth mAuth =  FirebaseAuth.getInstance();

    public void setTestInterface(OperationResultInterface testInterface) {
        mOperationResultInterface = testInterface;
    }

    public void loginUser(String email, String password, final Context context) {

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    persistTokenId();
                    FirebaseDatabase.getInstance().goOnline();
                    mOperationResultInterface.onSuccess();
                } else {
                    mOperationResultInterface.onFailure();
                }
            }
        });
    }

    private void persistTokenId() {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        String currentUserId = mAuth.getCurrentUser().getUid();
        AbstractAppCompatActivity.currentUserId = currentUserId;
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabase.child(currentUserId).child("device_token").setValue(deviceToken).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        });
    }



}
