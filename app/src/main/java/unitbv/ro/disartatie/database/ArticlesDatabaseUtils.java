package unitbv.ro.disartatie.database;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

import unitbv.ro.disartatie.database.abstracts.AbstractDatabaseUtil;

/**
 * Created by radua on 6/13/2018.
 */

public class ArticlesDatabaseUtils extends AbstractDatabaseUtil {

    public void postArticleBlog(String groupId, String currentUserId, String content, String picture) throws Exception {

        if (!TextUtils.isEmpty(content)) {

            DatabaseReference messagesReference = rootDatabaseReference.child("Groups").child(groupId).child("Articles");

            Map messageMap = new HashMap();
            messageMap.put("content", content);
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("articleWriter", currentUserId);
            messageMap.put("picture", picture);

            messagesReference.push().updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }
                }
            });

        }
    }


}
