package unitbv.ro.disartatie.database;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import unitbv.ro.disartatie.database.abstracts.AbstractDatabaseUtil;
import unitbv.ro.disartatie.enums.UserRelationship;
import unitbv.ro.disartatie.interfaces.FriendRequestsPostOperations;

import static unitbv.ro.disartatie.database.friendsDBRowNames.FRIENDS_SINCE;
import static unitbv.ro.disartatie.database.friendsDBRowNames.USER_RELATIONSHIP;

enum friendsDBRowNames {
    USER_RELATIONSHIP,
    FRIENDS_SINCE
}

public class FriendsDBUtils extends AbstractDatabaseUtil {

    private DatabaseReference friendRequestDatabase = rootDatabaseReference.child("Friends");
    private FirebaseAuth firebaseAuthInstance = FirebaseAuth.getInstance();
    private FriendRequestsPostOperations friendRequestsPostOperations;
    private NotificationsDatabase notificationsDatabase = new NotificationsDatabase();
    private final String currentUserId = firebaseAuthInstance.getUid();

    public void FriendsDBUtils() {
        friendRequestDatabase.keepSynced(true);
    }

    public void setFriendRequestsPostOperations(FriendRequestsPostOperations friendRequestsPostOperations) {
        this.friendRequestsPostOperations = friendRequestsPostOperations;
    }

    public void statusBetweenUsersListener(final String friendUserId) {
        friendRequestDatabase.child(currentUserId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!currentUserId.equals(friendUserId)) {
                    if (dataSnapshot.hasChild(friendUserId)) {
                        String requestType = dataSnapshot.child(friendUserId).child(USER_RELATIONSHIP.name()).getValue().toString();
                        if (requestType.equals(UserRelationship.REQUEST_RECEIVED.name())) {
                            friendRequestsPostOperations.onUserAwaitingApproval();
                        } else if (requestType.equals(UserRelationship.REQUEST_SENT.name())) {
                            friendRequestsPostOperations.onSendFriendRequestSuccesfull();
                        } else if (requestType.equals(UserRelationship.FRIENDS.name())) {
                            friendRequestsPostOperations.onAlreadyFriends();
                        }
                    } else {
                        friendRequestsPostOperations.onNotFriends();
                    }
                } else {
                    friendRequestsPostOperations.onOwnProfile();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void sendFriendRequest(final String friendUserId) {

        final String currentUserId = firebaseAuthInstance.getUid();
        Map requestMap = new HashMap();
        requestMap.put(currentUserId + "/" + friendUserId + "/" + USER_RELATIONSHIP.name(), UserRelationship.REQUEST_SENT);
        requestMap.put(friendUserId + "/" + currentUserId + "/" + USER_RELATIONSHIP.name(), UserRelationship.REQUEST_RECEIVED);
        friendRequestDatabase.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                notificationsDatabase.sendNotification(currentUserId, friendUserId);
                friendRequestsPostOperations.onSendFriendRequestSuccesfull();
            }
        });
    }

    public void deleteFriendRequest(final String friendUserId, final Boolean fromAccept) {
        friendRequestDatabase.child(currentUserId).child(friendUserId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override

            public void onSuccess(Void aVoid) {
                friendRequestDatabase.child(friendUserId).child(currentUserId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                            friendRequestsPostOperations.onNotFriends();
                    }
                });
            }
        });
    }

    public void acceptFriendRequest(final String friendUserId) {

        final String currentDate = DateFormat.getDateTimeInstance().format(new Date());
        Map acceptRequestMap = new HashMap();
        acceptRequestMap.put(currentUserId + "/" + friendUserId + "/" + FRIENDS_SINCE.name(), currentDate);
        acceptRequestMap.put(friendUserId + "/" + currentUserId + "/" + FRIENDS_SINCE.name(), currentDate);
        acceptRequestMap.put(currentUserId + "/" + friendUserId + "/" + USER_RELATIONSHIP.name(), UserRelationship.FRIENDS.name());
        acceptRequestMap.put(friendUserId + "/" + currentUserId + "/" + USER_RELATIONSHIP.name(), UserRelationship.FRIENDS.name());

        friendRequestDatabase.updateChildren(acceptRequestMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                friendRequestsPostOperations.onAlreadyFriends();
            }
        });
    }

    public void unfriendAction(final String friendUserId) {

        friendRequestDatabase.child(currentUserId).child(friendUserId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                friendRequestDatabase.child(friendUserId).child(currentUserId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        friendRequestsPostOperations.onNotFriends();
                    }
                });
            }
        });
    }
}
