package unitbv.ro.disartatie.database;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;
import java.util.Map;

import unitbv.ro.disartatie.database.abstracts.AbstractDatabaseUtil;

/**
 * Created by aradu on 27-Mar-18.
 */

public class UserDatabaseUtils extends AbstractDatabaseUtil {

    private DatabaseReference userDatabaseReference = rootDatabaseReference.child("Users");

    public void updateAvatarDatabaseReferences(String currentUserId, String imageUrl) {
        Map updateHashMap = new HashMap();
        updateHashMap.put("image", imageUrl);

        userDatabaseReference.child(currentUserId).updateChildren(updateHashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                }
            }
        });
    }

    public void updateUserStatus(DatabaseReference userDatabaseReference, String status, final Context context, final AlertDialog dialog) {
        userDatabaseReference.child("status").setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Status changed succesfully", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });
    }

    public void updateUserName(DatabaseReference userDatabaseReference, String name, final Context context, final AlertDialog dialog) {
        userDatabaseReference.child("name").setValue(name).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "User name changed succesfully", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });
    }

    public void updateUserDescription(DatabaseReference userDatabaseReference, String description, final Context context, final AlertDialog dialog) {
        userDatabaseReference.child("description").setValue(description).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Description changed succesfully", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }
            }
        });
    }

    public static String concatAndOrderLexicograficlyUserIds(String userId1, String userId2) throws Exception {
        int comparsionResult = userId1.compareTo(userId2);
        if (comparsionResult < 0) {
            return userId1 + "~" + userId2;
        } else if (comparsionResult > 0) {
            return userId2 + "~" + userId1;
        } else {
            throw new Exception("The user id's are identical");
        }
    }


}
