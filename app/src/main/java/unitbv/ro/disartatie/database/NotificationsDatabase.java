package unitbv.ro.disartatie.database;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;

import java.util.HashMap;

import unitbv.ro.disartatie.database.abstracts.AbstractDatabaseUtil;

/**
 * Created by radua on 3/30/2018.
 */

public class NotificationsDatabase extends AbstractDatabaseUtil {

    private DatabaseReference notificationsDatabase = rootDatabaseReference.child("Notifications");

    public void sendNotification(String currentUserId, String destinationUserId) {

        HashMap<String, String> notificationData = new HashMap<>();
        notificationData.put("from", currentUserId); //TODO
        notificationData.put("type", "request");
        notificationsDatabase.child(destinationUserId).push().setValue(notificationData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }

}
