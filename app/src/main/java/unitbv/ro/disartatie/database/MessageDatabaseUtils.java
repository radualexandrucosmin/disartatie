package unitbv.ro.disartatie.database;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;


public class MessageDatabaseUtils {

    public void sendMessage(String currentUserId, String message, DatabaseReference databaseReference) throws Exception {

        if (!TextUtils.isEmpty(message)) {


            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", currentUserId);

            databaseReference.push().updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }
                }
            });

        }
    }

    public void sendLocation(String currentUserId, double latitude, double longitude, DatabaseReference databaseReference) throws Exception {

        if (latitude != 0 && longitude != 0) {


            Map messageMap = new HashMap();
            messageMap.put("latitude", latitude);
            messageMap.put("longitude", longitude);
            messageMap.put("type", "location");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("from", currentUserId);

            databaseReference.push().updateChildren(messageMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError != null){

                        Log.d("CHAT_LOG", databaseError.getMessage().toString());

                    }
                }
            });

        }
    }
}
