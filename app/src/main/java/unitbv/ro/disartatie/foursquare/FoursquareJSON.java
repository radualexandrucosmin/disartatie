/**
 * Filename: FoursquareJSON.java
 * Author: Matthew Huie
 *
 * FoursquareJSON describes a JSON response from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

public class FoursquareJSON {

    // A response object within the JSON.
    public FoursquareResponse response;

}
