/**
 * Filename: FoursquareVenue.java
 * Author: Matthew Huie
 *
 * FoursquareVenue describes a venue object from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FoursquareVenue implements Serializable {

    // The ID of the category.
    public String id;
    // The name of the venue.
    public String name;
    // Contact details
    public FoursquareContact contact;
    // Status
    public FoursquareDefaultHours defaultHours;

    public String city;

    public String country;

    public String url;

    public FoursquareLocation location;

    // Subcategories
    public List<FoursquareCategory> categories = new ArrayList<>();

    public boolean loaded;
}