/**
 * Filename: FoursquareResults.java
 * Author: Matthew Huie
 *
 * FoursquareResults describes a results object from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

public class FoursquareResults {

    // A venue object within the results.
    FoursquareCategory venue;

}
