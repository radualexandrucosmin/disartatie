/**
 * Filename: FoursquareLocation.java
 * Author: Matthew Huie
 *
 * FoursquareLocation describes a location object from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

public class FoursquareIcon {

    // The address of the location.
    public String prefix;

    // The latitude of the location.
    public String suffix;

}