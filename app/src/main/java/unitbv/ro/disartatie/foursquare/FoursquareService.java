/**
 * Filename: FoursquareService.java
 * Author: Matthew Huie
 *
 * FoursquareService provides a Retrofit interface for the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface FoursquareService {

    @GET("venues/categories")
    Call<FoursquareJSON> getCategories(@Query("client_id")      String clientID,
                                       @Query("client_secret")  String clientSecret,
                                       @Query("v")              String version);


    @GET("venues/search")
    Call<FoursquareJSON> getNearbyVenues(@QueryMap Map<String, String> paramsMap);

    @GET("venues/{venueId}")
    Call<FoursquareJSON> getVenueDetails(@Path("venueId") String venueId,
                                         @QueryMap Map<String, String> paramsMap);

    @GET("venues/{venueId}/photos")
    Call<FoursquareJSON> getVenuePhotos(@Path("venueId") String venueId,
                                         @QueryMap Map<String, String> paramsMap);



}