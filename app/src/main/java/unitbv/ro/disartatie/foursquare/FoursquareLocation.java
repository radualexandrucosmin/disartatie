package unitbv.ro.disartatie.foursquare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FoursquareLocation implements Serializable {

    public String address;

    // The name of the venue.
    public String lat;

    // The plural name of the venue.
    public String lng;

    // The short name of the venue.
    public String distance;

    public String city;

    public String country;


    // Subcategories
    public List<FoursquareCategory> categories = new ArrayList<>();
}
