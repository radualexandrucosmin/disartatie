/**
 * Filename: FoursquareResponse.java
 * Author: Matthew Huie
 *
 * FoursquareResponse describes a response object from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FoursquareResponse implements Callback<FoursquareJSON> {

    // A group object within the response.
    FoursquareGroup group;
    public List<FoursquareCategory> categories = new ArrayList<>();

    public List<FoursquareVenue> venues = new ArrayList<>();

    public FoursquarePhoto photos = new FoursquarePhoto();

    public FoursquareVenue venue = new FoursquareVenue();

    @Override
    public void onResponse(Call<FoursquareJSON> call, Response<FoursquareJSON> response) {

    }

    @Override
    public void onFailure(Call<FoursquareJSON> call, Throwable t) {

    }
}