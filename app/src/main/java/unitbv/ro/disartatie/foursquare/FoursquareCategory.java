/**
 * Filename: FoursquareVenue.java
 * Author: Matthew Huie
 *
 * FoursquareVenue describes a venue object from the Foursquare API.
 */

package unitbv.ro.disartatie.foursquare;

import java.util.ArrayList;
import java.util.List;

public class FoursquareCategory {

    // The ID of the category.
    public String id;

    // The name of the venue.
    public String name;

    // The plural name of the venue.
    public String pluralName;

    // The short name of the venue.
    public String shortName;

    // The category Icon
    public FoursquareIcon icon;

    // Subcategories
    public List<FoursquareCategory> categories = new ArrayList<>();
}