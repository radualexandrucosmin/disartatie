package unitbv.ro.disartatie.adapters;

import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import unitbv.ro.disartatie.models.Article;
import unitbv.ro.disartatie.viewHolders.ArticleViewHolder;

public class BlogRecyclerAdapter extends RecyclerView.Adapter<ArticleViewHolder> {


    public List<Article> articleList;

    public BlogRecyclerAdapter(List<Article> articleList) {
        articleList = articleList;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

}