package unitbv.ro.disartatie.adapters;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.ar.SingleLocationARActivity;
import unitbv.ro.disartatie.utils.AndroidHacksUtils;
import unitbv.ro.disartatie.models.Message;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{


    private List<Message> messageList;
    private DatabaseReference userDatabase;


    public MessageAdapter(List<Message> messageList) {

        this.messageList = messageList;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout,parent, false);

        return new MessageViewHolder(v);

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText;
        public CircleImageView profileImage;
        public TextView displayName;
        public TextView messageTime;
        public LinearLayout messageLayout;
        public LinearLayout messageLayoutBackground;
        public LinearLayout messageLocation;
        public View currentView;

        public MessageViewHolder(View view) {
            super(view);
            this.currentView = view;
            messageLayoutBackground = view.findViewById(R.id.message_background_layout);
            messageLayout = view.findViewById(R.id.message_layout);
            messageText =  view.findViewById(R.id.message_text_layout);
            profileImage =  view.findViewById(R.id.message_profile_image);
            displayName = view.findViewById(R.id.name_text_layout);
            messageTime = view.findViewById(R.id.time_text_layouttime_text_layout);
            messageLocation = view.findViewById(R.id.message_location_layout);

        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

        Message message = messageList.get(i);

        String fromUser = message.getFrom();

        if (fromUser.equals(MainActivity.currentUserId)) {
            viewHolder.messageLayoutBackground.setBackgroundResource(R.drawable.round_black);
            viewHolder.messageLayout.removeView(viewHolder.profileImage);
            viewHolder.messageLayout.addView(viewHolder.profileImage, 1);
            viewHolder.messageLayout.setGravity(Gravity.RIGHT);
        } else {
            viewHolder.messageLayoutBackground.setBackgroundResource(R.drawable.round_blue);
            viewHolder.messageLayout.removeView(viewHolder.profileImage);
            viewHolder.messageLayout.addView(viewHolder.profileImage, 0);
            viewHolder.messageLayout.setGravity(Gravity.LEFT);
        }

        String messageType = message.getType();
        String userId = message.getFrom();

        userDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUser);

        userDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                StorageReference imageRef = FirebaseStorage.getInstance().getReference().child("profile_images").child("thumbs").child(MainActivity.currentUserId);
                Glide.with(viewHolder.currentView.getContext()).load(imageRef).placeholder(R.drawable.default_avatar).into(viewHolder.profileImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        viewHolder.messageTime.setText(AndroidHacksUtils.convertTime(message.getTime()));
        viewHolder.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("userId", userId);
                Navigation.findNavController(view).navigate(R.id.den_profile, bundle);
            }
        });

        if (messageType.equals("text")) {
            viewHolder.messageText.setText(message.getMessage());
        } else {
            Double latitude = message.getLatitude();
            Double longitude = message.getLongitude();
            viewHolder.messageText.setVisibility(View.GONE);
            viewHolder.messageLocation.setVisibility(View.VISIBLE);
            viewHolder.messageLocation.setOnClickListener((click) -> {
                Intent arCameraIntent = new Intent(viewHolder.currentView.getContext(), SingleLocationARActivity.class);
                arCameraIntent.putExtra("latitude", latitude);
                arCameraIntent.putExtra("longitude", longitude);
                arCameraIntent.putExtra("title", fromUser);
                viewHolder.currentView.getContext().startActivity(arCameraIntent);
            });
        }





    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }






}
