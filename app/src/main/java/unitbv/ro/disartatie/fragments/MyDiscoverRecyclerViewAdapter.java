package unitbv.ro.disartatie.fragments;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.ar.VenuesARActivity;
import unitbv.ro.disartatie.foursquare.FoursquareCategory;
import unitbv.ro.disartatie.fragments.DiscoverFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link FoursquareCategory} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyDiscoverRecyclerViewAdapter extends RecyclerView.Adapter<MyDiscoverRecyclerViewAdapter.ViewHolder> {

    private final List<FoursquareCategory> mValues;
    private final Context context;

    public MyDiscoverRecyclerViewAdapter(List<FoursquareCategory> items, Context context) {
        mValues = items;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_discover_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Glide.with(context).load(mValues.get(position).icon.prefix + 88 + mValues.get(position).icon.suffix).into(holder.mIconView);

        holder.mContentView.setText(mValues.get(position).name);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent myIntent = new Intent(context, VenuesARActivity.class);
                myIntent.putExtra("category", mValues.get(position).id); //Optional parameters
                context.startActivity(myIntent);

                }});
            }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mIconView;
        public final TextView mContentView;
        public FoursquareCategory mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIconView = (ImageView) view.findViewById(R.id.category_icon);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
