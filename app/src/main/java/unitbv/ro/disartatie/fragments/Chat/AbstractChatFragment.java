package unitbv.ro.disartatie.fragments.Chat;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.adapters.MessageAdapter;
import unitbv.ro.disartatie.database.MessageDatabaseUtils;
import unitbv.ro.disartatie.database.UserDatabaseUtils;
import unitbv.ro.disartatie.models.Message;

public abstract class AbstractChatFragment extends Fragment {

    @BindView(R.id.chat_send_btn) ImageButton chatSendBtn;
    @BindView(R.id.chat_message_view) EditText chatMessageView;
    @BindView(R.id.chat_messages_list) RecyclerView messagesRecyclerView;
    @BindView(R.id.chat_message_swipe_layout) SwipeRefreshLayout refreshLayout;
    @BindView(R.id.chat_location) ImageButton shareLocationBtn;

    // Parameters
    protected static final String CHAT_ID = "chatId";
    protected static final String CHAT_NAME = "chatName";
    protected String chatId;
    protected String chatName;
    protected UserDatabaseUtils userDatabaseUtils = new UserDatabaseUtils();
    protected String currentUserId;
    protected MessageDatabaseUtils messageDatabaseUtils = new MessageDatabaseUtils();

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

    private MessageAdapter messageAdapter;
    private final List<Message> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayout;

    private static final int TOTAL_ITEMS_TO_LOAD = 10;
    private int currentPage = 1;
    private int itemPos = 0;
    private String mLastKey = "";
    private String mPrevKey = "";


    protected abstract DatabaseReference getDatabaseReference() throws Exception;
    protected abstract void configButtons();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            chatId = getArguments().getString(CHAT_ID);
            chatName = getArguments().getString(CHAT_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        initialActivitySetup();
        configChatSendBtn();
        configRefreshAction();
        configButtons();

        try {
            loadMessages(getDatabaseReference());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void initialActivitySetup() {

        currentUserId = firebaseAuth.getCurrentUser().getUid();

        messageAdapter = new MessageAdapter(messagesList);

        linearLayout = new LinearLayoutManager(getActivity());
        messagesRecyclerView.setHasFixedSize(true);
        messagesRecyclerView.setLayoutManager(linearLayout);
        messagesRecyclerView.setAdapter(messageAdapter);

    }



    protected void configRefreshAction() {
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage++;

                itemPos = 0;

                try {
                    loadMoreMessages(getDatabaseReference());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void configChatSendBtn() {
        chatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String messageToSend = chatMessageView.getText().toString();
                    if (!messageToSend.isEmpty()) {
                        messageDatabaseUtils.sendMessage(currentUserId, chatMessageView.getText().toString(), getDatabaseReference());
                        chatMessageView.setText("");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void loadMessages(DatabaseReference messagesReference) throws Exception {

        messagesList.clear();
        Query messageQuery = messagesReference.limitToLast(currentPage * TOTAL_ITEMS_TO_LOAD);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Message message = dataSnapshot.getValue(Message.class);

                itemPos++;

                if(itemPos == 1){ //if it's the last message

                    String messageKey = dataSnapshot.getKey();

                    mLastKey = messageKey;
                    mPrevKey = messageKey;

                }

                messagesList.add(message);
                messageAdapter.notifyDataSetChanged();

                messagesRecyclerView.scrollToPosition(messagesList.size() - 1);
                //put's the view to the bottom, latest message

                refreshLayout.setRefreshing(false);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }

    private void loadMoreMessages(DatabaseReference messagesReference) {

        Query messageQuery = messagesReference.orderByKey().endAt(mLastKey).limitToLast(10);

        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                Message message = dataSnapshot.getValue(Message.class);
                String messageKey = dataSnapshot.getKey();

                if(!mPrevKey.equals(messageKey)){

                    messagesList.add(itemPos++, message);

                } else {

                    mPrevKey = mLastKey;

                }


                if(itemPos == 1) {

                    mLastKey = messageKey;

                }


                Log.d("TOTALKEYS", "Last Key : " + mLastKey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                messageAdapter.notifyDataSetChanged();

                refreshLayout.setRefreshing(false);

                linearLayout.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
