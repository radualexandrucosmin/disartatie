package unitbv.ro.disartatie.fragments.Den;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.models.Article;
import unitbv.ro.disartatie.viewHolders.ArticleViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class DenWall extends Fragment {
    private static final String GROUP_ID = "groupID";

    private String groupId;
    private DatabaseReference articlesDatabase;
    @BindView(R.id.blog_list_recycler_view)
    RecyclerView blogList;
    @BindView(R.id.add_new_post)
    FloatingActionButton addtoWall;

    public DenWall() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupId = getArguments().getString(GROUP_ID);
        }

        articlesDatabase = FirebaseDatabase.getInstance().getReference().child("Groups").child(groupId).child("Articles");
        articlesDatabase.keepSynced(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_den_wall, container, false);
        ButterKnife.bind(this, view);
        blogList.setHasFixedSize(true);
        blogList.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<Article> options = new FirebaseRecyclerOptions.Builder<Article>().setQuery(articlesDatabase, Article.class).build();

        FirebaseRecyclerAdapter<Article, ArticleViewHolder> articleRecyclerViewAdapter = new FirebaseRecyclerAdapter<Article, ArticleViewHolder>(options) {

            @Override
            public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_single_layout, parent, false);
                return new ArticleViewHolder(view);
            }


            @Override
            protected void onBindViewHolder(@NonNull final ArticleViewHolder holder, int position, @NonNull final Article model) {
                holder.setBlogDescription(model.getContent());
                holder.setBlogPostImage(model.getPicture(), getContext());
                holder.setBlogPostDate(DateUtils.getRelativeTimeSpanString(model.getTime()).toString());

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users").child(MainActivity.currentUserId).child("name");
                // Attach a listener to read the data at our posts reference
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        holder.setBlogUserName(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        System.out.println("The read failed: " + databaseError.getCode());
                    }
                });


            }


        };

        addtoWall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("groupID", groupId);
                Navigation.findNavController(view).navigate(R.id.den_create_post, bundle);
            }
        });

        blogList.setAdapter(articleRecyclerViewAdapter);
        articleRecyclerViewAdapter.startListening();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
