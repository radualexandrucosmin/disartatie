package unitbv.ro.disartatie.fragments.Den;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.database.ArticlesDatabaseUtils;

import static android.app.Activity.RESULT_OK;


public class CreateWallPostFragment extends DialogFragment {


    @BindView(R.id.create_post_image)
    ImageView createPostImage;
    @BindView(R.id.create_post_content)
    EditText createPostContent;
    @BindView(R.id.cancel_post_button)
    TextView cancelPostButton;
    @BindView(R.id.create_post_button)
    TextView createPostButton;

    private static final String GROUP_ID = "groupID";

    private String groupId;

    private Uri postImageURILocal = null ;
    private String postImageURIServer = null;

    private StorageReference mStorageReference;

    public CreateWallPostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            groupId = getArguments().getString(GROUP_ID);
        }

        mStorageReference = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_wall_post, container, false);
        ButterKnife.bind(this,view);

        createPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(2,1)
                        .setMinCropResultSize(512,512)
                        .getIntent(getContext());

                startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });

        createPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArticlesDatabaseUtils articlesDatabaseUtils = new ArticlesDatabaseUtils();
                try {
                    String content = createPostContent.getText().toString();

                    if(!TextUtils.isEmpty(content) && postImageURIServer != null) {
                        //TODO progress bar
                        articlesDatabaseUtils.postArticleBlog(groupId, MainActivity.currentUserId, createPostContent.getText().toString(), postImageURIServer);
                        Toast.makeText(getContext(), "Post succesfully saved.",
                                Toast.LENGTH_SHORT).show();
                        getParentFragmentManager().popBackStackImmediate();

                    } else {
                        Toast.makeText(getContext(), "Something went wrong",
                                Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        cancelPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if ( resultCode == RESULT_OK) {

                postImageURILocal = result.getUri();
                createPostImage.setImageURI(postImageURILocal);

                final String pushId = UUID.randomUUID().toString();

                StorageReference filepath = mStorageReference.child("wall_images").child(pushId);

                filepath.putFile(postImageURILocal).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        if (task.isSuccessful()) {

                            postImageURIServer = pushId;

                        }

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
