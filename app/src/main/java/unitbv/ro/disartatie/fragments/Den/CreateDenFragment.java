package unitbv.ro.disartatie.fragments.Den;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.utils.FirebaseStorageUtils;

import static android.app.Activity.RESULT_OK;

public class CreateDenFragment extends Fragment {

    public enum TYPE {
        PUBLIC,
        PRIVATE,
    }

    @BindView(R.id.create_den_image)
    ImageView denImage;
    @BindView(R.id.new_den_dialog_den_name)
    EditText denNameInputLayout;
    @BindView(R.id.new_den_dialog_den_description)
    EditText denDescriptionInputLayout;
    @BindView(R.id.new_den_dialog_den_latitude)
    EditText denLatitudeInput;
    @BindView(R.id.new_den_dialog_den_longitude)
    EditText denLongitudeInput;
    @BindView(R.id.new_den_dialog_den_create_button)
    TextView confirmDenCreationButton;
    @BindView(R.id.new_den_dialog_den_cancel_button)
    TextView cancelDenCreationButton;

    String placeType;

    private Uri resultUri;


    public CreateDenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_den, container, false);
        ButterKnife.bind(this, view);

        if (getArguments() != null) {
            placeType = getArguments().getString("type");
        }

        setupConfirmDenCreationButton();
        setupChangeImageButton();

        denLatitudeInput.setText(String.valueOf(MainActivity.latitude));
        denLongitudeInput.setText(String.valueOf(MainActivity.longitude));

        if(placeType.equals(TYPE.PRIVATE.toString())) {
            denLatitudeInput.setEnabled(true);
            denLongitudeInput.setEnabled(true);
        } else {
            denLatitudeInput.setEnabled(false);
            denLongitudeInput.setEnabled(false);
        }

        cancelDenCreationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getParentFragmentManager().popBackStackImmediate();
            }
        });

        if (getArguments() != null) {
            placeType = getArguments().getString("type");
        }
        return view;
    }

    private void setupConfirmDenCreationButton() {
        confirmDenCreationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resultUri != null) {
                    final String name = denNameInputLayout.getText().toString();
                    final String description = denDescriptionInputLayout.getText().toString();
                    final DatabaseReference denReference;

                    if(placeType.equals(TYPE.PRIVATE.toString())) {
                        denReference = FirebaseDatabase.getInstance().getReference().child("Users").child(MainActivity.currentUserId).child("Spots").push();
                    } else {
                        denReference = FirebaseDatabase.getInstance().getReference().child("Groups").push();
                    }

                    final Map newDenMap = new HashMap();
                    newDenMap.put("name", name);
                    newDenMap.put("description", description);
                    newDenMap.put("time", ServerValue.TIMESTAMP);
                    newDenMap.put("owner", FirebaseAuth.getInstance().getCurrentUser().getUid());
                    newDenMap.put("latitude", MainActivity.latitude);
                    newDenMap.put("longitude", MainActivity.longitude);
                    newDenMap.put("id", denReference.getKey());

                    denReference.setValue(newDenMap);

                    denReference.updateChildren(newDenMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if (databaseError != null) {
                                Log.d("CHAT_LOG", databaseError.getMessage().toString());
                            } else {
                                final StorageReference avatarFilePath = FirebaseStorage.getInstance().getReference().child("den_images").child(databaseReference.getKey());
                                avatarFilePath.putFile(resultUri);
                                getParentFragmentManager().popBackStackImmediate();
                            }



                        }
                    });
                } else {
                    Toast.makeText(getContext(), "Please select an image", Toast.LENGTH_LONG);
                }


            }

        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupChangeImageButton() {
        denImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(2,1)
                        .setMinCropResultSize(512,256)
                        .getIntent(getContext());

                startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FirebaseStorageUtils firebaseStorageUtils = new FirebaseStorageUtils();
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                Glide.with(getView()).load(resultUri).into(denImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


}
