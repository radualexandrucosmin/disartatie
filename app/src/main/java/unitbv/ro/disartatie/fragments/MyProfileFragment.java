package unitbv.ro.disartatie.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.database.UserDatabaseUtils;
import unitbv.ro.disartatie.utils.FirebaseStorageUtils;

import static android.app.Activity.RESULT_OK;

public class MyProfileFragment extends Fragment {

    private DatabaseReference mUserDatabase;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

    @BindView(R.id.settings_image)
    ImageView mDisplayImage;
    @BindView(R.id.settings_name_edit)
    ImageView editUserName;
    @BindView(R.id.settings_status_edit)
    ImageView editUserStatus;
    @BindView(R.id.settings_description_edit)
    ImageView editDescription;
    @BindView(R.id.settings_name)
    TextView userName;
    @BindView(R.id.settings_status)
    TextView userStatus;
    @BindView(R.id.settings_description)
    TextView userDescription;



   public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String uid = currentUser.getUid();
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        mUserDatabase.keepSynced(true); //Firebase offline capabilities


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_settings, container, false);
        ButterKnife.bind(this, view);
        setupChangeStatusButton();
        setupChangeTitleButton();
        setupChangeImageButton();
        setupChangeDescriptionButton();
        setupDatabaseListener();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupDatabaseListener() {
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String displayName = dataSnapshot.child("name").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();
                if(dataSnapshot.child("description").getValue() != null) {
                    String description = dataSnapshot.child("description").getValue().toString();
                    userDescription.setText(description);
                }


                userName.setText(displayName);
                userStatus.setText(status);

                StorageReference imageRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(image);

                Glide.with(getContext()).load(imageRef).into(mDisplayImage);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void setupChangeStatusButton() {
        editUserStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mView = getLayoutInflater().inflate(R.layout.dialog_change_status, null);

                final TextView dialogTitle = mView.findViewById(R.id.dialog_change_status_title);
                final TextInputLayout textInputLayout = mView.findViewById(R.id.dialog_change_status_edit_layout);
                final Button confirmStatusChangeButton = mView.findViewById(R.id.dialog_confirm_status_change_button);
                dialogTitle.setText("Change your status");

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                confirmStatusChangeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String status = textInputLayout.getEditText().getText().toString();
                        UserDatabaseUtils userDatabaseUtils = new UserDatabaseUtils();
                        userDatabaseUtils.updateUserStatus(mUserDatabase, status, getActivity() , dialog);
                    }
                });
            }
        });
    }

    private void setupChangeTitleButton() {
        editUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mView = getLayoutInflater().inflate(R.layout.dialog_change_status, null);
                final TextView dialogTitle = mView.findViewById(R.id.dialog_change_status_title);
                final TextInputLayout textInputLayout = mView.findViewById(R.id.dialog_change_status_edit_layout);
                final Button confirmStatusChangeButton = mView.findViewById(R.id.dialog_confirm_status_change_button);
                dialogTitle.setText("Change your name");
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                confirmStatusChangeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String name = textInputLayout.getEditText().getText().toString();
                        UserDatabaseUtils userDatabaseUtils = new UserDatabaseUtils();
                        userDatabaseUtils.updateUserName(mUserDatabase, name, getActivity() , dialog);
                    }
                });
            }
        });
    }

    private void setupChangeDescriptionButton() {
        editDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mView = getLayoutInflater().inflate(R.layout.dialog_change_status, null);
                final TextInputLayout textInputLayout = mView.findViewById(R.id.dialog_change_status_edit_layout);
                final TextView dialogTitle = mView.findViewById(R.id.dialog_change_status_title);
                final Button confirmStatusChangeButton = mView.findViewById(R.id.dialog_confirm_status_change_button);
                dialogTitle.setText("Change your description");

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                confirmStatusChangeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String description = textInputLayout.getEditText().getText().toString();
                        UserDatabaseUtils userDatabaseUtils = new UserDatabaseUtils();
                        userDatabaseUtils.updateUserDescription(mUserDatabase, description, getActivity() , dialog);
                    }
                });
            }
        });
    }

    private void setupChangeImageButton() {
        mDisplayImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                        .setAspectRatio(1,1)
                        .setMinCropResultSize(512,512)
                        .getIntent(getContext());

                startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FirebaseStorageUtils firebaseStorageUtils = new FirebaseStorageUtils();
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                firebaseStorageUtils.uploadAvatarToDatabase(result, getActivity());
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


}
