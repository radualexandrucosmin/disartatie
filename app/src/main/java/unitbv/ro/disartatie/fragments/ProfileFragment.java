package unitbv.ro.disartatie.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.database.FriendsDBUtils;
import unitbv.ro.disartatie.enums.UserRelationship;
import unitbv.ro.disartatie.interfaces.FriendRequestsPostOperations;

import static android.view.View.GONE;


public class ProfileFragment extends Fragment implements FriendRequestsPostOperations {

    @BindView(R.id.settings_name) TextView userName;
    @BindView(R.id.settings_status) TextView userStatus;
    @BindView(R.id.settings_image)
    ImageView userAvatar;
    @BindView(R.id.profile_first_button) Button friendStatusButton;
    @BindView(R.id.profile_second_button) Button friendDeclineButton;
    @BindView(R.id.settings_name_edit)
    ImageView editUserName;
    @BindView(R.id.settings_status_edit)
    ImageView editUserStatus;
    @BindView(R.id.settings_description_edit)
    ImageView editDescription;

    private UserRelationship currentState;
    private DatabaseReference mUserDatabase;
    private FriendsDBUtils friendRequestsDBUtils;
    private String friendUserId;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "userId";

    // TODO: Rename and change types of parameters
    private String userId;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_settings, container, false);
        ButterKnife.bind(this, view);

        editDescription.setVisibility(GONE);
        editUserName.setVisibility(GONE);
        editUserStatus.setVisibility(GONE);

        friendStatusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentState.equals(UserRelationship.NOT_FRIENDS)) {
                    sendFriendRequest(friendUserId);
                } else if(currentState.equals(UserRelationship.REQUEST_SENT)) {
                    //Don't do nothing, button is disabled in status Awaiting Approval
                } else if(currentState.equals(UserRelationship.REQUEST_RECEIVED)) {
                    friendStatusButton.setBackgroundColor(Color.GREEN);
                    acceptFriendRequest(friendUserId);
                } else if(currentState.equals(UserRelationship.FRIENDS)) {
                    Toast.makeText(getActivity(), "User functionality for messaging user", Toast.LENGTH_LONG).show();
                }
            }
        });

        friendDeclineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentState.equals(UserRelationship.NOT_FRIENDS)) {
                    Toast.makeText(getActivity(), "User functionality for block user", Toast.LENGTH_LONG).show();
                } else if(currentState.equals(UserRelationship.REQUEST_SENT)) {
                    cancelFriendRequest(friendUserId);
                } else if(currentState.equals(UserRelationship.REQUEST_RECEIVED)) {
                    friendDeclineButton.setBackgroundColor(Color.GREEN);
                    cancelFriendRequest(friendUserId);
                } else if(currentState.equals(UserRelationship.FRIENDS)) {
                    unfriendAction(friendUserId);
                }
            }
        });
        initialSetup();
        setupDatabaseListener();

        return view;
    }

    @NonNull
    private void initialSetup() {
        friendUserId = userId;
        friendRequestsDBUtils = new FriendsDBUtils();
        friendRequestsDBUtils.setFriendRequestsPostOperations(this);
        currentState = UserRelationship.NOT_FRIENDS;
        friendRequestsDBUtils.statusBetweenUsersListener(friendUserId);
        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(friendUserId);
        mUserDatabase.keepSynced(true);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void sendFriendRequest(String uid) {
        friendRequestsDBUtils.sendFriendRequest(uid);
    }

    private void cancelFriendRequest(String uid) {
        friendRequestsDBUtils.deleteFriendRequest(uid, false);
    }

    private void acceptFriendRequest(String uid) {
        friendRequestsDBUtils.acceptFriendRequest(uid);
    }

    private void unfriendAction(String uid) {
        friendRequestsDBUtils.unfriendAction(uid);
    }

    private void setupDatabaseListener() {
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String displayName = dataSnapshot.child("name").getValue().toString();
                String status = dataSnapshot.child("status").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();


                userName.setText(displayName);
                userStatus.setText(status);

                if (!image.equals("default")) {
                    Glide.with(getContext()).load(image).into(userAvatar);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onSendFriendRequestSuccesfull() {
        currentState = UserRelationship.REQUEST_SENT;
        setupFirstButtonForUI(false, "Awaiting Approval");
        setupSecondButtonForUI(true, "Cancel request");
    }

    @Override
    public void onUserAwaitingApproval() {
        currentState = UserRelationship.REQUEST_RECEIVED;
        setupFirstButtonForUI(true, "Accept request");
        setupSecondButtonForUI(true, "Decline request");
    }

    @Override
    public void onAlreadyFriends() {
        currentState = UserRelationship.FRIENDS;
        setupFirstButtonForUI(true, "Say Hi!");
        setupSecondButtonForUI(true, "Unfriend");
    }

    @Override
    public void onNotFriends() {
        currentState = UserRelationship.NOT_FRIENDS;
        setupFirstButtonForUI(true,"Add Friend");
        setupSecondButtonForUI(true,"Block user");
    }

    @Override
    public void onOwnProfile() {
        currentState = UserRelationship.OWN_PROFILE;
        setupFirstButtonForUI(false, "It's you :)");
        setupSecondButtonForUI(false, "Go to profile");
    }

    private void setupFirstButtonForUI(Boolean buttonEnabled, String buttonText ) {
        friendStatusButton.setEnabled(buttonEnabled);
        friendStatusButton.setText(buttonText);
    }

    private void setupSecondButtonForUI(Boolean buttonEnabled, String buttonText ) {
        friendDeclineButton.setEnabled(buttonEnabled);
        friendDeclineButton.setText(buttonText);
    }


}
