package unitbv.ro.disartatie.fragments.Chat;


import android.view.View;
import android.widget.Button;

import androidx.fragment.app.Fragment;

import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.database.UserDatabaseUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivateChatFragment extends AbstractChatFragment {

    @BindView(R.id.show_den_wall)
    Button showDenWallButton;


    public PrivateChatFragment() {
        // Required empty public constructor
    }

    @Override
    protected DatabaseReference getDatabaseReference() throws Exception {

        DatabaseReference dbRef = userDatabaseUtils.rootDatabaseReference.child("Messages").child(UserDatabaseUtils.concatAndOrderLexicograficlyUserIds(currentUserId,chatId));
        return  dbRef;
    }

    @Override
    protected void configButtons() {
        showDenWallButton.setVisibility(View.GONE);
        shareLocationBtn.setOnClickListener((element) -> {
            try {
                messageDatabaseUtils.sendLocation(currentUserId, MainActivity.latitude, MainActivity.longitude, getDatabaseReference());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }




}
