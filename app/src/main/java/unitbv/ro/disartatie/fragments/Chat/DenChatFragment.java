package unitbv.ro.disartatie.fragments.Chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.navigation.Navigation;

import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;


public class DenChatFragment extends AbstractChatFragment {

    @BindView(R.id.show_den_wall)
    Button showDenWallButton;
    @BindView(R.id.chat_location)
    ImageView shareLocationButton;


    public DenChatFragment() {
        // Required empty public constructor
    }



    @Override
    protected DatabaseReference getDatabaseReference() {
        return userDatabaseUtils.rootDatabaseReference.child("Groups").child(chatId).child("Messages");
    }

    @Override
    protected void configButtons() {
        shareLocationButton.setVisibility(View.GONE);
        showDenWallButton.setOnClickListener((view) -> {
                Bundle bundle = new Bundle();
                bundle.putString("groupID", chatId);
                bundle.putString("userID", MainActivity.currentUserId);
                Navigation.findNavController(view).navigate(R.id.den_wall, bundle);
        });
    }
}
