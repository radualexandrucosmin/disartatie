package unitbv.ro.disartatie.fragments.Den;


import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.ar.SingleLocationARActivity;
import unitbv.ro.disartatie.models.Den;
import unitbv.ro.disartatie.viewHolders.DenViewHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivateSpotsFragment extends Fragment {

    RecyclerView privateSpots;
    private DatabaseReference densDatabase;
    private View mainView;
    FloatingActionButton actionButton;

    public PrivateSpotsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_private, container, false);

        privateSpots = mainView.findViewById(R.id.dens_list_fragment);
        privateSpots.setHasFixedSize(true);
        privateSpots.setLayoutManager(new LinearLayoutManager(getContext()));


        actionButton = mainView.findViewById(R.id.add_new_private_spot);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("type", CreateDenFragment.TYPE.PRIVATE.toString());
                Navigation.findNavController(v).navigate(R.id.den_create_den, bundle);
            }
        });

        densDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(MainActivity.currentUserId).child("Spots");

        // Inflate the layout for this fragment
        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        getPrivateSpots();


    }

    private void getPrivateSpots() {
        FirebaseRecyclerOptions<Den> options = new FirebaseRecyclerOptions.Builder<Den>().setQuery(densDatabase, Den.class).build();

        FirebaseRecyclerAdapter<Den, DenViewHolder> densRecyclerViewAdapter = new FirebaseRecyclerAdapter<Den, DenViewHolder>(options) {


            @Override
            protected void onBindViewHolder(@NonNull final DenViewHolder holder, int position, @NonNull final Den model) {

                holder.setTitle(model.getName());
                holder.setDenImage(model, getActivity());
                holder.setDescription(model.getDescription());

                final String groupId = getRef(position).getKey();

                Location targetLocation = new Location(""); //provider name is unnecessary
                targetLocation.setLatitude(model.getLatitude()); //your coords of course
                targetLocation.setLongitude(model.getLongitude());

                Location myLocation = new Location("");
                myLocation.setLatitude(MainActivity.latitude);
                myLocation.setLongitude(MainActivity.longitude);

                holder.setDistance(targetLocation, myLocation);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setupDensViewHolder(model.getLatitude(), model.getLongitude(), model.getName(), holder, groupId);
                    }
                });

            }

            @Override
            public DenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.den_single_layout, parent, false);
                return new DenViewHolder(view);
            }


        };

        privateSpots.setAdapter(densRecyclerViewAdapter);
        densRecyclerViewAdapter.startListening();
    }

    private void setupDensViewHolder(final Double latitude, final Double longitude, final String name, @NonNull RecyclerView.ViewHolder holder, final String list_user_id) {
        holder.itemView.setFocusable(false);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent arCameraIntent = new Intent(getContext(), SingleLocationARActivity.class);
                arCameraIntent.putExtra("latitude", latitude);
                arCameraIntent.putExtra("longitude", longitude);
                arCameraIntent.putExtra("title", name);
                getContext().startActivity(arCameraIntent);
            }
        });
    }

}
