package unitbv.ro.disartatie.fragments.Den;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.ar.DensArActivity;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.models.Den;
import unitbv.ro.disartatie.viewHolders.DenViewHolder;


/**
 * A simple {@link Fragment} subclass.
 */
public class DenFragment extends Fragment {

    RecyclerView densList;
    Button createDenButton;
    Button showDenMapButton;
    ImageButton refreshDenListButton;
    private DatabaseReference densDatabase;
    private View mainView;

    public DenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_den, container, false);

        densList = mainView.findViewById(R.id.dens_list_fragment);
        createDenButton = mainView.findViewById(R.id.create_den);
        showDenMapButton = mainView.findViewById(R.id.show_den_map);
        refreshDenListButton = mainView.findViewById(R.id.refresh_den_list_map);

        densDatabase = FirebaseDatabase.getInstance().getReference().child("Groups");

        densList.setHasFixedSize(true);
        densList.setLayoutManager(new LinearLayoutManager(getContext()));

        createDenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putString("type", CreateDenFragment.TYPE.PUBLIC.toString());
                Navigation.findNavController(v).navigate(R.id.den_create_den, bundle);
            }
        });

        showDenMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent denMapsIntent = new Intent(getContext(), DensArActivity.class);
                startActivity(denMapsIntent);
            }
        });

        refreshDenListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocalDensList();
            }
        });

        // Inflate the layout for this fragment
        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        getLocalDensList();
    }

    private void getLocalDensList() {
        FirebaseRecyclerOptions<Den> options = new FirebaseRecyclerOptions.Builder<Den>().setQuery(densDatabase, Den.class).build();

        FirebaseRecyclerAdapter<Den, DenViewHolder> densRecyclerViewAdapter = new FirebaseRecyclerAdapter<Den, DenViewHolder>(options) {


            @Override
            protected void onBindViewHolder(@NonNull final DenViewHolder holder, int position, @NonNull final Den model) {

                holder.setTitle(model.getName());
                holder.setDenImage(model, getActivity());
                holder.setDescription(model.getDescription());

                final String groupId = getRef(position).getKey();

                Location targetLocation = new Location(""); //provider name is unnecessary
                targetLocation.setLatitude(model.getLatitude()); //your coords of course
                targetLocation.setLongitude(model.getLongitude());

                Location myLocation = new Location("");
                myLocation.setLatitude(MainActivity.latitude);
                myLocation.setLongitude(MainActivity.longitude);

                holder.setDistance(targetLocation, myLocation);

                float distanceInMeters = targetLocation.distanceTo(myLocation);

                if (distanceInMeters > 1000) {
                    holder.setVisibility(false);
                } else {
                    holder.setVisibility(true);
                    if(distanceInMeters < 200) {
                        holder.itemView.findViewById(R.id.den_single_distance_background).setBackground(getResources().getDrawable(R.drawable.green_rect, null));
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setupDensViewHolder(model.getName(), model.getDescription(), holder, groupId);
                            }
                        });
                    } else {
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(getContext(),"You need to be near this spot in order to access it.",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }


            }

            @Override
            public DenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.den_single_layout, parent, false);
                return new DenViewHolder(view);
            }


        };

        densList.setAdapter(densRecyclerViewAdapter);
        densRecyclerViewAdapter.startListening();
    }

    private void setupDensViewHolder(final String userName, final String description, @NonNull RecyclerView.ViewHolder holder, final String list_user_id) {
        holder.itemView.setFocusable(false);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("chatId", list_user_id);
                bundle.putString("chatName", userName);
                bundle.putString("chatDescription", description);
                Navigation.findNavController(view).navigate(R.id.den_chat, bundle);
            }
        });
    }

}
