package unitbv.ro.disartatie.activities.abstracts;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import unitbv.ro.disartatie.services.GPSTracking;

import static android.content.ContentValues.TAG;

/**
 * Created by radua on 6/5/2018.
 */

public abstract class AbstractAppCompatActivity extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    public static double longitude;
    public static double latitude;
    public static String currentUserId;

    @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null) {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    longitude = intent.getDoubleExtra("gpsLongitude", 0);
                    latitude = intent.getDoubleExtra("gpsLatitude", 0);
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
}
