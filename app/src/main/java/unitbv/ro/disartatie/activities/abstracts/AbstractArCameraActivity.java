package unitbv.ro.disartatie.activities.abstracts;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.ArSceneView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.LocationScene;
import uk.co.appoly.arcorelocation.rendering.LocationNode;
import uk.co.appoly.arcorelocation.utils.ARLocationPermissionHelper;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.abstracts.AbstractAppCompatActivity;
import unitbv.ro.disartatie.utils.AugmentedRealityLocationUtils;
import unitbv.ro.disartatie.utils.DistanceUtils;
import unitbv.ro.disartatie.utils.PermissionUtils;

public abstract class AbstractArCameraActivity extends AbstractAppCompatActivity {

    protected boolean areAllMarkersLoaded = false;
    protected Handler arHandler = new Handler(Looper.getMainLooper());
    // Our ARCore-Location scene
    protected LocationScene locationScene = null;
    @BindView(R.id.ar_scene_view)
    ArSceneView arSceneView;
    private boolean arCoreInstallRequested = false;

    public void setupMarkerAspect(LocationMarker locationMarker, View layoutRendarable, LocationNode locationNode) {

    };


    // Abstract methods
    public void configureData() {

    };

    public void configureLayoutViews() {

    };

    public void retrieveIntentParams()  {

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arcamera);
        ButterKnife.bind(this);
        retrieveIntentParams();
        configureLayoutViews();
        configureData();
    }

    /**
     * Make sure we call locationScene.pause();
     */
    @Override
    public void onPause() {
        super.onPause();
        if (locationScene != null) {
            locationScene.pause();
            arSceneView.pause();
        }
    }


    protected void computeNewScaleModifierBasedOnDistance(LocationMarker locationMarker, int distance) {
        Float scaleModifier = AugmentedRealityLocationUtils.getScaleModifierBasedOnRealDistance(distance);
        if (scaleModifier == AugmentedRealityLocationUtils.INVALID_MARKER_SCALE_MODIFIER) {
            detachMarker(locationMarker);
        } else {
            locationMarker.setScaleModifier(scaleModifier);
        }
    }

    protected void processFrameAndMarkers() {

        arSceneView.getScene().addOnUpdateListener(frameTime -> {
            if (!areAllMarkersLoaded) {
                return;
            }

            locationScene.mLocationMarkers.forEach(locationMarker -> {
                Location markerLocation = new Location("");
                markerLocation.setLatitude(locationMarker.latitude);
                markerLocation.setLongitude(locationMarker.longitude);
                Location myLocation = new Location("");
                myLocation.setLatitude(this.latitude);
                myLocation.setLongitude(this.longitude);
                myLocation.distanceTo(markerLocation);
                float height = DistanceUtils.generateRandomHeightBasedOnDistance(myLocation.distanceTo(markerLocation));
                locationMarker.setHeight(height);
            });

            Frame frame = arSceneView.getArFrame();

            if (frame == null) {
                return;
            }

            if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
                return;
            }

            if (locationScene != null) {
                locationScene.processFrame(frame);
            }
        });

    }


    protected void attachMarkerToScene(LocationMarker locationMarker, View layoutRendarable) {

        locationScene.resume();
        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException e) {
            e.printStackTrace();
        }

        locationMarker.setScalingMode(LocationMarker.ScalingMode.FIXED_SIZE_ON_SCREEN);
        Location markerLocation = new Location("");
        markerLocation.setLatitude(locationMarker.latitude);
        markerLocation.setLongitude(locationMarker.longitude);
        Location myLocation = new Location("");
        myLocation.setLatitude(this.latitude);
        myLocation.setLongitude(this.longitude);

        //tre calculat distanta
        locationMarker.setScaleModifier(DistanceUtils.getScaleModifierBasedOnRealDistance(myLocation.distanceTo(markerLocation)));

        locationScene.mLocationMarkers.add(locationMarker);
        if (locationMarker.anchorNode != null) {
            locationMarker.anchorNode.setEnabled(true);
        }


        arHandler.post(() -> {
            locationScene.refreshAnchors();
            layoutRendarable.findViewById(R.id.pinContainer).setVisibility(View.VISIBLE);
        });

        locationMarker.setRenderEvent(locationNode -> {
            setupMarkerAspect(locationMarker, layoutRendarable, locationNode);
        });
    }

    private void checkAndRequestPermissions() {
        if (!PermissionUtils.hasLocationAndCameraPermissions(this)) {
            PermissionUtils.requestCameraAndLocationPermissions(this);
        } else {
            setupSession();
        }
    }

    private void detachMarker(LocationMarker locationMarker) {
        locationMarker.anchorNode.getAnchor().detach();
        locationMarker.anchorNode.setEnabled(false);
        locationMarker.anchorNode = null;
    }


    private void setupSession() {
        if (arSceneView == null) {
            return;
        }

        if (arSceneView.getSession() == null) {
            try {
                Session session = AugmentedRealityLocationUtils.setupSession(this, arCoreInstallRequested);
                if (session == null) {
                    arCoreInstallRequested = true;
                    return;
                } else {
                    arSceneView.setupSession(session);
                }
            } catch (UnavailableException exception) {
                AugmentedRealityLocationUtils.handleSessionException(this, exception);
            }
        }

        if (locationScene == null) {
            locationScene = new LocationScene(this, arSceneView);
            locationScene.setMinimalRefreshing(true);
            locationScene.setOffsetOverlapping(true);
            locationScene.setAnchorRefreshInterval(2000);
        }

        locationScene.resume();
        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        if (!ARLocationPermissionHelper.hasPermission(this)) {
            if (!ARLocationPermissionHelper.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                ARLocationPermissionHelper.launchPermissionSettings(this);
            } else {
                Toast.makeText(
                        this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                        .show();
            }
            finish();
        }
    }



    /**
     * Make sure we call locationScene.resume();
     */
    @Override
    protected void onResume() {
        super.onResume();
        checkAndRequestPermissions();
    }

}
