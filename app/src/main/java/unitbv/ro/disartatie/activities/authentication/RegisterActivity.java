package unitbv.ro.disartatie.activities.authentication;

import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.abstracts.AbstractAppCompatActivity;
import unitbv.ro.disartatie.authenticator.RegisterUserAuthenticator;
import unitbv.ro.disartatie.interfaces.OperationResultInterface;
import unitbv.ro.disartatie.utils.Connectivity;

public class RegisterActivity extends AbstractAppCompatActivity implements OperationResultInterface {

    @BindView(R.id.register_username) EditText mDisplayName;
    @BindView(R.id.register_email) EditText mEmail;
    @BindView(R.id.register_password) EditText mPassword;
    @BindView(R.id.register_button) TextView mCreateAccountButton;
    @BindView(R.id.register_go_to_login) TextView mGoToLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mGoToLogin.setOnClickListener((element) -> {
            Intent mainIntent = new Intent(RegisterActivity.this, LoginActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            this.startActivity(mainIntent);
            finish();
        });

        mCreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String displayName = mDisplayName.getText().toString();
                String email = mEmail.getText().toString();
                String password = mPassword.getText().toString();

                if(!displayName.isEmpty() && !email.isEmpty() && !password.isEmpty()) {

                    if(Connectivity.isConnected(getApplicationContext())) {
                        RegisterUserAuthenticator registerUserAuthenticator = new RegisterUserAuthenticator();
                        registerUserAuthenticator.setOperationResultInterface(RegisterActivity.this);
                        registerUserAuthenticator.registerUser(displayName,email,password, RegisterActivity.this);
                    } else {
                        Toast.makeText(getApplicationContext(),"Please connect to the internet", Toast.LENGTH_LONG).show();
                    }


                }
            }
        });

    }

    @Override
    public void onSuccess() {
        Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,"Failed to register user.", Toast.LENGTH_LONG).show();
        //TODO facut sa afiseze tipul de eroare aparuta in backend
    }

}
