package unitbv.ro.disartatie.activities.ar;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.rendering.LocationNode;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.abstracts.AbstractArCameraActivity;
import unitbv.ro.disartatie.models.Den;
import unitbv.ro.disartatie.utils.AugmentedRealityLocationUtils;

public class SingleLocationARActivity extends AbstractArCameraActivity {


    private List<Den> densList = new ArrayList<>();

    @BindView(R.id.arcamera_text_info)
    TextView den_text_info;

    @BindView(R.id.ar_camera_bottom_bar)
    LinearLayout arcamera_activity_status_bar;

    Double latitude;
    Double longitude;
    String title;

    @Override
    public void configureLayoutViews() {
        arcamera_activity_status_bar.setVisibility(View.GONE);
    }

    public void retrieveIntentParams() {
        Intent intent = getIntent();

        latitude = intent.getDoubleExtra("latitude", 0);
        longitude = intent.getDoubleExtra("longitude", 0);
        title = intent.getStringExtra("title");
    }

    private void setupAndRenderVenuesMarkers() {
            CompletableFuture<ViewRenderable> completableFutureViewRenderable =
                    ViewRenderable.builder()
                            .setView(this, R.layout.venue_item_ar_layout)
                            .build();

            CompletableFuture.anyOf(completableFutureViewRenderable).handle((obiectu, throwable) -> {
                        if (throwable != null) {
                            return null;
                        }
                        try {
                            LocationMarker venueMarker = new LocationMarker(
                                    longitude,
                                    latitude,
                                    setVenueNode(longitude, latitude, title, completableFutureViewRenderable)
                            );

                            arHandler.postDelayed(() -> {
                                try {
                                    attachMarkerToScene(
                                            venueMarker,
                                            completableFutureViewRenderable.get().getView()
                                    );
                                        areAllMarkersLoaded = true;
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                            }, 200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
            );
    }



    public void setupMarkerAspect(LocationMarker locationMarker, View layoutRendarable, LocationNode locationNode) {
        TextView distanceText = layoutRendarable.findViewById(R.id.distance);
        distanceText.setText(AugmentedRealityLocationUtils.showDistance(locationNode.getDistance()));
        computeNewScaleModifierBasedOnDistance(locationMarker, locationNode.getDistance());
    }


    private Node setVenueNode(Double longitude, Double latitude, String name, CompletableFuture<ViewRenderable> completableFuture) throws ExecutionException, InterruptedException {
        Node node = new Node();
        node.setRenderable(completableFuture.get());

        View nodeLayout = completableFuture.get().getView();

        TextView venueNameTextView = nodeLayout.findViewById(R.id.name);
        RelativeLayout markerLayoutContainer = nodeLayout.findViewById(R.id.pinContainer);
        venueNameTextView.setText(name);
        markerLayoutContainer.setVisibility(View.GONE);

        nodeLayout.setOnTouchListener((v, event) -> {
            Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+ latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
            return false;
        });
        return node;
    }

}
