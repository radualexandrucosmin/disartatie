package unitbv.ro.disartatie.activities.ar;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ViewRenderable;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.rendering.LocationNode;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.abstracts.AbstractArCameraActivity;
import unitbv.ro.disartatie.models.Den;
import unitbv.ro.disartatie.utils.AugmentedRealityLocationUtils;

public class DensArActivity extends AbstractArCameraActivity{


    private List<Den> densList = new ArrayList<>();

    @BindView(R.id.arcamera_text_info)
    TextView den_text_info;

    @BindView(R.id.ar_camera_bottom_bar)
    LinearLayout arcamera_activity_status_bar;


    @Override
    public void configureData() {


        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Groups");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Den den = postSnapshot.getValue(Den.class);
                    densList.add(den);
                }

                areAllMarkersLoaded = false;
                locationScene.clearMarkers();
                setupAndRenderVenuesMarkers();
                processFrameAndMarkers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // ?
            }
        });
    }

    public void confireLayoutViews() {
        arcamera_activity_status_bar.setVisibility(View.GONE);
    }

    public void retrieveIntentParams() {
        //no params needed
    }

    private void setupAndRenderVenuesMarkers() {
        densList.forEach(den -> {
            CompletableFuture<ViewRenderable> completableFutureViewRenderable =
                    ViewRenderable.builder()
                            .setView(this, R.layout.venue_item_ar_layout)
                            .build();

            CompletableFuture.anyOf(completableFutureViewRenderable).handle((obiectu, throwable) -> {
                        if (throwable != null) {
                            return null;
                        }
                        try {
                            LocationMarker venueMarker = new LocationMarker(
                                    den.getLongitude(),
                                    den.getLatitude(),
                                    setVenueNode(den, completableFutureViewRenderable)
                            );

                            arHandler.postDelayed(() -> {
                                try {
                                    attachMarkerToScene(
                                            venueMarker,
                                            completableFutureViewRenderable.get().getView()
                                    );
                                    if (densList.indexOf(den) == densList.size() - 1) {
                                        areAllMarkersLoaded = true;
                                    }
                                } catch (InterruptedException | ExecutionException e) {
                                    e.printStackTrace();
                                }

                            }, 200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
            );
        });
    }


    public void setupMarkerAspect(LocationMarker locationMarker, View layoutRendarable, LocationNode locationNode) {
        TextView distanceText = layoutRendarable.findViewById(R.id.distance);
        distanceText.setText(AugmentedRealityLocationUtils.showDistance(locationNode.getDistance()));
        computeNewScaleModifierBasedOnDistance(locationMarker, locationNode.getDistance());
    }


    private Node setVenueNode(Den den, CompletableFuture<ViewRenderable> completableFuture) throws ExecutionException, InterruptedException {
        Node node = new Node();
        node.setRenderable(completableFuture.get());

        View nodeLayout = completableFuture.get().getView();

        TextView venueNameTextView = nodeLayout.findViewById(R.id.name);
        RelativeLayout markerLayoutContainer = nodeLayout.findViewById(R.id.pinContainer);
        venueNameTextView.setText(den.getName());
        markerLayoutContainer.setVisibility(View.GONE);

        nodeLayout.setOnTouchListener((v, event) -> {
            Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+ den.getLatitude() + "," + den.getLongitude());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
            return false;
        });
        return node;
    }

}
