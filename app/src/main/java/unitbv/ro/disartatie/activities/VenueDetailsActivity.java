package unitbv.ro.disartatie.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.foursquare.FoursquareJSON;
import unitbv.ro.disartatie.foursquare.FoursquarePhotoItem;
import unitbv.ro.disartatie.foursquare.FoursquareResponse;
import unitbv.ro.disartatie.foursquare.FoursquareService;
import unitbv.ro.disartatie.foursquare.FoursquareVenue;

public class VenueDetailsActivity extends AppCompatActivity implements Callback<FoursquareJSON> {


    private Map<String, String> apiQueryParams = new HashMap<>();
    private String venueId;
    private FoursquareVenue venue;
    @BindView(R.id.venue_details_photo) ImageView venuePhoto;
    @BindView(R.id.venue_details_name) TextView venueTitle;
    @BindView(R.id.venue_details_status) TextView venueStatus;
    @BindView(R.id.venue_details_address) TextView venueAddress;
    @BindView(R.id.venue_details_contact) TextView venueContact;
    @BindView(R.id.venue_details_go_to_map) ImageView navigateToMap;
    @BindView(R.id.venue_details_go_to_street) ImageView navigateToStreetView;
    @BindView(R.id.venue_details_go_to_site) ImageView navigateToWebsite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_details);
        Intent intent = getIntent();
        venueId = intent.getStringExtra("venueId");
        ButterKnife.bind(this);
        setupRetrofit();
    }

    private void setupButtons() {
        navigateToWebsite.setOnClickListener(v -> this.goToVenueWebsite());

        navigateToMap.setOnClickListener(v -> this.goToVenueMaps());

        navigateToStreetView.setOnClickListener(v -> this.goToStreetView());

    }

    private void goToVenueWebsite() {
        if(venue != null) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(venue.url)));
        }
    }

    private void goToVenueMaps() {
        if(venue != null) {
            // Create a Uri from an intent string. Use the result to create an Intent.
            Uri gmmIntentUri = Uri.parse("geo:"+ venue.location.lat + "," + venue.location.lng);
            // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            // Make the Intent explicit by setting the Google Maps package
            mapIntent.setPackage("com.google.android.apps.maps");
            // Attempt to start an activity that can handle the Intent
            startActivity(mapIntent);
        }
    }

    private void goToStreetView() {
        if(venue != null) {
            // Displays an image of the Swiss Alps
            Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+ venue.location.lat + "," + venue.location.lng);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
    }

    private void setupRetrofit() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FoursquareService foursquareAPI = retrofit.create(FoursquareService.class);

        apiQueryParams.put("client_id", getResources().getString(R.string.foursquare_client_id));
        apiQueryParams.put("client_secret", getResources().getString(R.string.foursquare_client_secret));
        apiQueryParams.put("v", "20200305");


        foursquareAPI.getVenueDetails(venueId, apiQueryParams).enqueue(this);
        foursquareAPI.getVenuePhotos(venueId, apiQueryParams).enqueue(this);//
    }

    @Override
    public void onResponse(Call<FoursquareJSON> call, Response<FoursquareJSON> response) {

        FoursquareResponse value = response.body().response;

        if(value.venue != null && value.venue.name != null) {
            venue = response.body().response.venue;
            venueTitle.setText(venue.name);
            if (venue.defaultHours != null && venue.defaultHours.status != null) {
                venueStatus.setText(venue.defaultHours.status);
            }
            if (venue.location != null && venue.location.address != null) {
                venueAddress.setText(venue.location.address);
            }
            venueContact.setText((venue.contact.formattedPhone != null ? venue.contact.formattedPhone : venue.contact.phone));
            setupButtons();
        } else if(value.photos != null && value.photos.items.length > 0) {
                FoursquarePhotoItem photo = value.photos.items[0];
                Glide.with(this.getApplicationContext()).load(photo.prefix + "600x400" + photo.suffix).into(venuePhoto);
            }

        }

    @Override
    public void onFailure(Call<FoursquareJSON> call, Throwable t) {
        //TODO - log the error
    }
}
