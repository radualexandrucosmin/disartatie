/*
 * Copyright 2018 Google LLC. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package unitbv.ro.disartatie.activities.ar;
import android.content.Intent;
import android.net.Uri;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.rendering.ViewRenderable;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.appoly.arcorelocation.LocationMarker;
import uk.co.appoly.arcorelocation.rendering.LocationNode;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.VenueDetailsActivity;
import unitbv.ro.disartatie.activities.abstracts.AbstractArCameraActivity;
import unitbv.ro.disartatie.foursquare.FoursquareJSON;
import unitbv.ro.disartatie.foursquare.FoursquareService;
import unitbv.ro.disartatie.foursquare.FoursquareVenue;
import unitbv.ro.disartatie.utils.AugmentedRealityLocationUtils;

public class VenuesARActivity extends AbstractArCameraActivity implements Callback<FoursquareJSON> {



    private FoursquareService foursquareAPI;

    private Map<String, String> apiQueryParams = new HashMap<>();

    private List<FoursquareVenue> venues = new ArrayList<>();


    private String categoryId;

    private FoursquareVenue currentlySelectedVenue;


    @BindView(R.id.arcamera_text_info)
    TextView arcamera_text_info;

    @BindView(R.id.arcamera_activity_details_button)
    ImageView arcamera_activity_details_button;

    @BindView(R.id.arcamera_activity_maps_button)
    ImageView arcamera_activity_maps_button;

    @BindView(R.id.arcamera_activity_streetview_button)
    ImageView arcamera_activity_streetview_button;


    @Override
    public void retrieveIntentParams() {
        categoryId = getIntent().getStringExtra("category");
    }

    // BEGIN - DATA RETRIEVAL

    @Override
    public void configureData() {
        apiQueryParams.put("client_id", getResources().getString(R.string.foursquare_client_id));
        apiQueryParams.put("client_secret", getResources().getString(R.string.foursquare_client_secret));
        apiQueryParams.put("v", "20200305");
        apiQueryParams.put("limit", "20");
        apiQueryParams.put("categoryId", categoryId);
        apiQueryParams.put("ll", MainActivity.latitude + "," + MainActivity.longitude);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.foursquare.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        foursquareAPI = retrofit.create(FoursquareService.class);

        foursquareAPI.getNearbyVenues(apiQueryParams).enqueue(this);
    }

    @Override
    public void onResponse(Call<FoursquareJSON> call, Response<FoursquareJSON> response) {
        FoursquareJSON venueWrapper = response.body();
        if(venueWrapper.response != null && venueWrapper.response.venues != null) {
            venues.clear();
            venues.addAll(venueWrapper.response.venues);
            areAllMarkersLoaded = false;
            locationScene.clearMarkers();
            setupAndRenderVenuesMarkers();
            processFrameAndMarkers();
        }
    }



    @Override
    public void onFailure(Call<FoursquareJSON> call, Throwable t) {

    }
    // END - DATA RETRIEVAL

    // BEGIN - Configure layouts and buttons
    @Override
    public void configureLayoutViews() {
        ButterKnife.bind(this);

        arcamera_activity_details_button.setOnClickListener(v -> {
            this.goToVenueDetailsActivity();
        });

        arcamera_activity_maps_button.setOnClickListener(v -> {
            this.goToVenueMaps();
        });

        arcamera_activity_streetview_button.setOnClickListener(v -> {
            this.goToStreetView();
        });
    }


    private void goToVenueDetailsActivity() {
        if(currentlySelectedVenue != null) {
            Intent intent = new Intent(this, VenueDetailsActivity.class);
            intent.putExtra("venueId", currentlySelectedVenue.id);
            startActivity(intent);
        } else {
            arcamera_text_info.setText("Please select a venue");
        }
    }

    private void goToVenueMaps() {
        if(currentlySelectedVenue != null) {
            // Create a Uri from an intent string. Use the result to create an Intent.
            Uri gmmIntentUri = Uri.parse("geo:"+ currentlySelectedVenue.location.lat + "," + currentlySelectedVenue.location.lng);
            // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            // Make the Intent explicit by setting the Google Maps package
            mapIntent.setPackage("com.google.android.apps.maps");
            // Attempt to start an activity that can handle the Intent
            startActivity(mapIntent);
        } else {
            arcamera_text_info.setText("Please select a venue");
        }
    }

    private void goToStreetView() {
        if(currentlySelectedVenue != null) {
            Uri gmmIntentUri = Uri.parse("google.streetview:cbll="+ currentlySelectedVenue.location.lat + "," + currentlySelectedVenue.location.lng);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        } else {
            arcamera_text_info.setText("Please select a venue");
        }
    }

    // END - Configure layouts and buttons





    public void setupMarkerAspect(LocationMarker locationMarker, View layoutRendarable, LocationNode locationNode) {
        TextView distanceText = layoutRendarable.findViewById(R.id.distance);
        distanceText.setText(AugmentedRealityLocationUtils.showDistance(locationNode.getDistance()));
        computeNewScaleModifierBasedOnDistance(locationMarker, locationNode.getDistance());
    }

    private void setupAndRenderVenuesMarkers() {

        venues.forEach(foursquareVenue -> {
            CompletableFuture<ViewRenderable> completableFutureViewRenderable =
                    ViewRenderable.builder()
                            .setView(this, R.layout.venue_item_ar_layout)
                            .build();

            CompletableFuture.anyOf(completableFutureViewRenderable).handle((obiectu, throwable) -> {
                        if (throwable != null) {
                            return null;
                        }
                        try {
                            LocationMarker venueMarker = new LocationMarker(
                                    Double.parseDouble(foursquareVenue.location.lng),
                                    Double.parseDouble(foursquareVenue.location.lat),
                                    configureVenueNode(foursquareVenue, completableFutureViewRenderable)
                            );

                            arHandler.postDelayed(() -> {
                                try {
                                    attachMarkerToScene(
                                            venueMarker,
                                            completableFutureViewRenderable.get().getView()
                                    );
                                    if (venues.indexOf(foursquareVenue) == venues.size() - 1) {
                                        areAllMarkersLoaded = true;
                                    }
                                } catch (ExecutionException | InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }, 200);
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                return null;
                    }
            );
        });
    }


    private Node configureVenueNode(FoursquareVenue venue, CompletableFuture<ViewRenderable> completableFuture) throws ExecutionException, InterruptedException {
        Node node = new Node();
        node.setRenderable(completableFuture.get());

        View nodeLayout = completableFuture.get().getView();

        TextView venueNameTextView = nodeLayout.findViewById(R.id.name);
        RelativeLayout markerLayoutContainer = nodeLayout.findViewById(R.id.pinContainer);
        venueNameTextView.setText(venue.name);
        markerLayoutContainer.setVisibility(View.GONE);

        nodeLayout.setOnTouchListener((v, event) -> {
            arcamera_text_info.setText(venue.name);
            currentlySelectedVenue = venue;
            return false;
        });
        return node;
    }

}