package unitbv.ro.disartatie.activities.authentication;

import android.Manifest;
import android.content.Intent;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.activities.MainActivity;
import unitbv.ro.disartatie.activities.abstracts.AbstractAppCompatActivity;
import unitbv.ro.disartatie.authenticator.LoginUserAuthenticator;
import unitbv.ro.disartatie.interfaces.OperationResultInterface;
import unitbv.ro.disartatie.services.GPSTracking;
import unitbv.ro.disartatie.utils.Connectivity;

import static android.content.ContentValues.TAG;

public class LoginActivity extends AbstractAppCompatActivity implements OperationResultInterface {


    @BindView(R.id.login_email) EditText mLoginEmail;
    @BindView(R.id.login_password) EditText mLoginPassword;
    @BindView(R.id.login_button) TextView mLoginButton;
    @BindView(R.id.login_go_to_sign_up) TextView mGoToSignUp;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        goToMainActivityIfUserIsAuthenticated();
        mGoToSignUp.setOnClickListener(element -> {
            Intent mainIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            this.startActivity(mainIntent);
            finish();
        });


        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mLoginEmail.getText().toString();
                String password = mLoginPassword.getText().toString();

                if (!email.isEmpty() && !password.isEmpty()) {
                    if(Connectivity.isConnected(getApplicationContext())) {
                        LoginUserAuthenticator loginAuthenticator = new LoginUserAuthenticator();
                        loginAuthenticator.setTestInterface(LoginActivity.this);
                        loginAuthenticator.loginUser(email, password, LoginActivity.this);
                        changeButtonsState(false);
                    } else {
                        Toast.makeText(getApplicationContext(),"Please connect to the internet", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }

    private void goToMainActivityIfUserIsAuthenticated() {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() != null ) {
            this.currentUserId = mAuth.getCurrentUser().getUid();
            Intent intent = new Intent(this, MainActivity.class);
            initializeLocationListener();
            startActivity(intent);
        }

    }

    @Override
    public void onSuccess() {
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        initializeLocationListener();
        this.startActivity(mainIntent);
        finish();
    }

    @Override
    public void onFailure() {
        changeButtonsState(true);
        Toast.makeText(this, "Error", Toast.LENGTH_LONG);
    }

    private void changeButtonsState(boolean isEnabled) {
        if(!isEnabled) {
            mLoginButton.setText("Logging you in, please wait.");
        } else {
            mLoginButton.setText("Login");
        }

        mLoginButton.setEnabled(isEnabled);
        mGoToSignUp.setEnabled(isEnabled);
    }

    private void initializeLocationListener() {

        if (!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) )


            {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    12314);
        }

        Intent intent = new Intent(this.getApplicationContext(), GPSTracking.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//add this line
        this.getApplicationContext().startService(intent);
    }



}
