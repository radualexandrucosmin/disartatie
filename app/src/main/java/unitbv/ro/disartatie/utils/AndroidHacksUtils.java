package unitbv.ro.disartatie.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.io.IOUtils.copy;

public class AndroidHacksUtils {

    public static byte[] loadBitmap(String url) throws IOException {
        URL newurl = new URL(url);
        Bitmap avatarBitmap = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        avatarBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        avatarBitmap.recycle();
        return byteArray;
    }

    public static String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }

}


