package unitbv.ro.disartatie.utils;

import java.util.Random;

public class DistanceUtils {

    public final static double AVERAGE_RADIUS_OF_EARTH_KM = 6371;

    /*The haversine formula determines the great-circle distance between two points on a sphere given their longitudes and latitudes.
    Important in navigation, it is a special case of a more general formula in spherical trigonometry, the law of haversines, that relates the sides and angles of spherical triangles.
    */
//     public static int calculateDistanceInKilometer(double userLat, double userLng,
//                                            double venueLat, double venueLng) {
//
//        double latDistance = Math.toRadians(userLat - venueLat);
//        double lngDistance = Math.toRadians(userLng - venueLng);
//
//        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
//                + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
//                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);
//
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//
//        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH_KM * c));
//    }

    public static boolean isBetween(float x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    public static float randFloat(float min, float max) {
        Random rand = new Random();
        return rand.nextFloat() * (max - min) + min;
    }

    public static Float generateRandomHeightBasedOnDistance(float distance) {

        if(isBetween(distance, 0, 1000)) return randFloat(1,3);
        else if(isBetween(distance, 1001, 1500)) return randFloat(4,6);
        else if(isBetween(distance, 1501, 2000)) return randFloat(7,9);
        else if(isBetween(distance, 2001, 3000)) return randFloat(10,12);
        else if(distance > 3000) return randFloat(12,13);
        else return 0f;
    }

    public static String showDistance(int distance) {
        if (distance >= 1000)
            return String.format("%.2f", ((double) distance / 1000)) + " km";
        else
            return distance + "m";
    }

    public static Float getScaleModifierBasedOnRealDistance(float distance) {

        if(distance < 5) return -1f;
        else if(isBetween(distance, 5, 20)) return 0.8f;
        else if(isBetween(distance, 21, 60)) return 0.75f;
        else if(isBetween(distance, 61, 80)) return 0.7f;
        else if(isBetween(distance, 81, 100)) return 0.65f;
        else if(isBetween(distance, 101, 1000)) return 0.6f;
        else if(isBetween(distance, 1001, 1500)) return 0.5f;
        else if(isBetween(distance, 1501, 2000)) return 0.45f;
        else if(isBetween(distance, 2001, 2500)) return 0.4f;
        else if(isBetween(distance, 2501, 3000)) return 0.35f;
        else if(isBetween(distance, 3001, 7000)) return 0.25f;
        else if(distance > Integer.MAX_VALUE) return 0.15f;
        else return -1f;
    }
}


