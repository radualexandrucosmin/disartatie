package unitbv.ro.disartatie.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import id.zelory.compressor.Compressor;
import unitbv.ro.disartatie.database.MessageDatabaseUtils;
import unitbv.ro.disartatie.database.UserDatabaseUtils;


public class FirebaseStorageUtils {

    StorageReference mStorageRef = FirebaseStorage.getInstance().getReference();
    FirebaseUser mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
    UserDatabaseUtils userDatabaseUtils = new UserDatabaseUtils();
    MessageDatabaseUtils messageDatabaseUtils = new MessageDatabaseUtils();

    public void uploadAvatarToDatabase(CropImage.ActivityResult result, Context context) {
        Uri resultUri = result.getUri();
        String userId = mCurrentUser.getUid();
        final StorageReference avatarFilePath = mStorageRef.child("profile_images").child(userId);
        final StorageReference avatarThumbFilePath = mStorageRef.child("profile_images").child("thumbs").child(userId);
        final byte[] compressedAvatar = compressUploadedAvatar(new File(resultUri.getPath()), context);

        uploadFileToServer(resultUri, avatarFilePath, avatarThumbFilePath, compressedAvatar, userId);
    }


    private byte[] compressUploadedAvatar(File resultUri, Context context) {
        Bitmap compressedAvatar = null;
        try {
            compressedAvatar = new Compressor(context).setMaxWidth(200).setMaxHeight(200).setQuality(75).compressToBitmap(resultUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        compressedAvatar.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        return baos.toByteArray();
    }


    private void uploadFileToServer(Uri resultUri, StorageReference avatarFilePath, final StorageReference avatarThumbFilePath, final byte[] compressedAvatar, String userID) {
        avatarFilePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    //TODO method that will create the downlaodUrl based on uid
                    UploadTask uploadTask = avatarThumbFilePath.putBytes(compressedAvatar);
                    uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumbTask) {
                            String thumbDownloadUrl = thumbTask.getResult().getUploadSessionUri().toString();
                            if (thumbTask.isSuccessful()) {
                                userDatabaseUtils.updateAvatarDatabaseReferences(mCurrentUser.getUid(), userID);
                            } else {

                            }
                        }
                    });
                }
            }
        });
    }

}
