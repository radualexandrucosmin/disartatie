package unitbv.ro.disartatie.interfaces;

/**
 * Created by radua on 3/22/2018.
 */

public interface OperationResultInterface {

    void onSuccess();
    void onFailure();

}
