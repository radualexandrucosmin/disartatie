package unitbv.ro.disartatie.interfaces;

/**
 * Created by radua on 3/29/2018.
 */

public interface FriendRequestsPostOperations {

    void onSendFriendRequestSuccesfull();
    void onUserAwaitingApproval();
    void onAlreadyFriends();
    void onNotFriends();
    void onOwnProfile();
}
