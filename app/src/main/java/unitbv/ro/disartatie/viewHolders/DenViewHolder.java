package unitbv.ro.disartatie.viewHolders;

import android.content.Context;
import android.location.Location;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import unitbv.ro.disartatie.R;
import unitbv.ro.disartatie.utils.MD5Util;
import unitbv.ro.disartatie.models.Den;

/**
 * Created by radua on 4/12/2018.
 */

public class DenViewHolder extends RecyclerView.ViewHolder {

    View view;

    public DenViewHolder(View itemView) {
        super(itemView);

        view = itemView;

    }
    public void setVisibility(boolean isVisible){
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams)itemView.getLayoutParams();
        if (isVisible){
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        }else{
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }

    public void setDistance(Location denLocation, Location currentLocation) {

        TextView denPositionView = (TextView) view.findViewById(R.id.den_single_distance);
        float distanceInKm = denLocation.distanceTo(currentLocation);
        denPositionView.setText(distanceInKm +"\nmeters");

    }

    public void setDenImage(Den model, Context ctx) {


        ImageView denImageView = view.findViewById(R.id.den_single_image);
        if(model.getId() != null) {
            StorageReference avatarFilePath = FirebaseStorage.getInstance().getReference().child("den_images").child(model.getId());
            Glide.with(ctx).load(avatarFilePath).into(denImageView);
        } else {
            String placeholderImage = "https://www.gravatar.com/avatar/" + MD5Util.md5Hex(model.getName() + model.getDescription()) + "?d=identicon";
            Glide.with(ctx).load(placeholderImage).into(denImageView);
        }




    }

    public void setTitle(String title) {

        TextView denTitleView = (TextView) view.findViewById(R.id.den_single_title);
        denTitleView.setText(title);

    }

    public void setDescription(String description) {

        TextView denDescriptionView = (TextView) view.findViewById(R.id.den_single_description);
        denDescriptionView.setText(description);

    }

}
