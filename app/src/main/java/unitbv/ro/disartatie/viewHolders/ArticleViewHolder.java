package unitbv.ro.disartatie.viewHolders;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import de.hdodenhof.circleimageview.CircleImageView;
import unitbv.ro.disartatie.R;

public class ArticleViewHolder extends RecyclerView.ViewHolder {

    View view;

    public ArticleViewHolder(View itemView) {
        super(itemView);

        view = itemView;
    }

    public void setInvisible() {
        itemView.setVisibility(View.GONE);
    }

    public void setVisible() {
        itemView.setVisibility(View.GONE);
    }

//    public void setLocation(String latitude, String longitude) {
//
//        TextView denPositionView = (TextView) view.findViewById(R.id.den_position);
//        denPositionView.setText("Latitude: " + latitude + " | Longitude: " + longitude);
//
//    }

    public void setBlogUserName(String userName) {

        TextView blogUserNameView = (TextView) view.findViewById(R.id.blog_user_name);
        blogUserNameView.setText(userName);

    }

    public void setBlogUserImage(String imageUrl, Context ctx) {

        CircleImageView blogUserImage = (CircleImageView) view.findViewById(R.id.blog_user_image);
        Glide.with(ctx).load(imageUrl).into(blogUserImage);
    }

    public void setBlogPostDate(String date) {

        TextView blogPostDate = (TextView) view.findViewById(R.id.blog_date);
        blogPostDate.setText(date);

    }

    public void setBlogPostImage(String imageUrl, Context ctx) {

        ImageView blogUserImage = view.findViewById(R.id.blog_image);
        StorageReference image = FirebaseStorage.getInstance().getReference().child("wall_images").child(imageUrl);
        Glide.with(ctx).load(image).into(blogUserImage);
    }

    public void setBlogDescription(String description) {

        TextView blogPostDescription = (TextView) view.findViewById(R.id.blog_desc);
        blogPostDescription.setText(description);

    }


}
