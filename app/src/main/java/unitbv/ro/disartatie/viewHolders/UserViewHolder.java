package unitbv.ro.disartatie.viewHolders;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import unitbv.ro.disartatie.R;

public class  UserViewHolder extends RecyclerView.ViewHolder {
    View mView;

    public UserViewHolder(View itemView) {
        super(itemView);
        mView = itemView;
    }

    public void setName(String name) {
        TextView mUserName = mView.findViewById(R.id.user_single_name);
        mUserName.setTextColor(Color.WHITE);
        mUserName.setText(name);
    }

    public void setUserStatus(String status){

        TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
        userStatusView.setTextColor(Color.WHITE);
        userStatusView.setText(status);


    }

    public void setUserImage(String thumb_image) {

        ImageView userImageView = (ImageView) mView.findViewById(R.id.user_avatar);
//        Glide.with(ctx).load(thumb_image).into(userImageView);

    }
}
