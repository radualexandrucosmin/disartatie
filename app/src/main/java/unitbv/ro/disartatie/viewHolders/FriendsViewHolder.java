package unitbv.ro.disartatie.viewHolders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import unitbv.ro.disartatie.R;

public class FriendsViewHolder extends RecyclerView.ViewHolder {

    View view;

    public FriendsViewHolder(View itemView) {
        super(itemView);

        view = itemView;

    }

    public void setDate(String date) {

        TextView userStatusView = (TextView) view.findViewById(R.id.user_single_status);
        userStatusView.setText(date);

    }

    public void setName(String name) {

        TextView userNameView = (TextView) view.findViewById(R.id.user_single_name);
        userNameView.setText(name);

    }

    public void setUserSingleStatus(String status) {

        TextView userStatusView = (TextView) view.findViewById(R.id.user_single_status);
        userStatusView.setText(status);

    }

    public void setUserImage(String thumb_image, Context ctx) {

        ImageView userImageView = view.findViewById(R.id.user_avatar);
        Glide.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);
    }


    public void setUserOnline(String online_status) {

        ImageView userOnlineView = (ImageView) view.findViewById(R.id.user_single_online_icon);

        if (online_status.equals("true")) {

            userOnlineView.setVisibility(View.VISIBLE);

        } else {

            userOnlineView.setVisibility(View.INVISIBLE);

        }

    }


}
