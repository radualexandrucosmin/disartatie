package unitbv.ro.disartatie.viewHolders;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import unitbv.ro.disartatie.R;

/**
 * Created by radua on 4/4/2018.
 */

public class MessageViewHolder extends RecyclerView.ViewHolder {

    View view;

    public MessageViewHolder(View itemView) {
        super(itemView);
        view = itemView;
    }

    public void setName(String name) {
        TextView mUserName = view.findViewById(R.id.name_text_layout);
        mUserName.setText(name);
    }

    public void setTime(String time) {
        TextView timeText = view.findViewById(R.id.time_text_layouttime_text_layout);
        timeText.setText(time);
    }

}
